// When page and it's assets are fully loaded
jQuery(document).ready(function($) {
    
    var chessboardjs_drupal_init = function() {
        // Get HTML element we're going to convert to chessboard.
        var html_board = document.getElementById('chessboardjs_board');
        // Get FEN string from HTML element data- property.
        var fen_string = html_board.dataset.fenString;
        // Set pieces pics URL masl (default URLs are wrong, if lib is not
        // located at site's root folder).
        
        // TODO: Get module loacation from drupal for non-standard installation
        // folders
        var chess_pieces_url_mask = 'http://' + window.location.host + '/sites/all/modules/chessboardjs/chessboardjs/img/chesspieces/wikipedia/{piece}.png';
        
        var chess_config = {
            // Set chessboard theme.
            pieceTheme: chess_pieces_url_mask,
            // Set current pieces position.
            position: fen_string
        };
        
        // Draw chess board.
        var chessboardjs_board = ChessBoard('chessboardjs_board', chess_config);
    };
    
    chessboardjs_drupal_init();
});